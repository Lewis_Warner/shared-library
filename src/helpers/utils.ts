import { IUser } from "@warnster/shared-interfaces";
import { v4 as uuid } from "uuid";
import { UserDaoClient } from "../daos/user/user_client_dao";

export const STEP_SETUP_URL = '/setup-steps'

export const getInitials = (name = "") => {
  return name
    .replace(/\s+/, " ")
    .split(" ")
    .slice(0, 2)
    .map((v) => v && v[0].toUpperCase())
    .join("");
};

//The url to direct a user to when visiting "/"
export const getRedirectUrlForLogin = (docUser: IUser): string => {
  let redirectUrl = "/";
  //email verified and profile is complete, redirect to customer homepage or admin
  if (docUser.profileComplete === true) {
    redirectUrl = "/profiles";
    if (docUser.isAdmin === true) {
      redirectUrl = "/admin/users";
    }
  }
  //email verified and profile is incomplete, redirect to customer profile form
  if (!docUser.profileComplete) {
    redirectUrl = STEP_SETUP_URL;
  }
  return redirectUrl;
};

export const getRedirectUrl = async (user: IUser): Promise<string> => {
  const userDao = new UserDaoClient()
  const firebaseUser = await userDao.getCurrentUser();
  let redirectUrl = "/";
  if (firebaseUser) {
    //email verified and profile is complete, redirect to customer homepage or admin
    if (firebaseUser.emailVerified && user.profileComplete === true) {
      redirectUrl = "/profiles";
      if (user.isAdmin === true) {
        redirectUrl = "/admin/users";
      }
    }
    //email verified and profile is incomplete, redirect to customer profile form
    if (!user.profileComplete) {
      redirectUrl = "/profile-pending";
    }
  } else {
    redirectUrl = "/login";
  }
  return redirectUrl;
};

export const bytesToSize = (bytes: number, decimals = 2) => {
  if (bytes === 0) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};

export const getBase64 = async (file: File): Promise<string | ArrayBuffer | null | undefined> => {
  const reader = new FileReader()
  return new Promise(resolve => {
    reader.onload = e => {
      resolve(e?.target?.result)
    }
    reader.readAsDataURL(file)
  })
}

const objFromArray = (arr: any, key = "id") =>
  arr.reduce((accumulator: any, current: any) => {
    accumulator[current[key]] = current;
    return accumulator;
  }, {});



export default objFromArray;

export const uniqueUID = () => {
  return uuid();
}
