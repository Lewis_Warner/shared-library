import moment from "moment";

export const getMoment = (date: string | Date) => {
  return moment(date).local();
};

export const formatAppDate = (date: moment.Moment) => {
  return date.format("dddd, MMMM Do YYYY, h:mm:ss a");
};

export const formatShortAppDate = (date: moment.Moment) => {
  return date.format('DD/MM/YY')
}

export const getAge = (date: moment.Moment) => {
  return date.fromNow(true).split(" ")[0]
}