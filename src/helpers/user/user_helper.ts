import { IProfile, IProfileFirebase, IUser, IUserRetrievedFirebase } from "@warnster/shared-interfaces";
import firebase from 'firebase';

export const transformFirebaseUser = (firebaseUser: IUserRetrievedFirebase): IUser => {
    return { ...firebaseUser, createdAt: firebaseUser.createdAt.toDate(), updatedAt: firebaseUser.updatedAt.toDate() };
};

export const transformFirebaseProfile = (firebaseProfile: IProfileFirebase, profileID: string): IProfile => {
    let avatar = firebaseProfile.avatar || '';
    if (avatar === "" && firebaseProfile.images.length > 0) {
        avatar = firebaseProfile.images[0].url
    }
    return { ...firebaseProfile, createdAt: firebaseProfile.createdAt.toDate(), updatedAt: firebaseProfile.updatedAt.toDate(), profileID, avatar }
}

export const isFacebookOrTwitter = (firebaseUser: firebase.User) => {
    const result = firebaseUser.providerData.find(d => d && (d.providerId === 'facebook.com' || d.providerId === 'twitter.com'))
    return result ? true : false
}

export const CurrencyToSymbol: { [userID: string]: string } = {
    gbp: "£",
    usd: "$"
}