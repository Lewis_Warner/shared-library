import { IComment, ICommentFirebase, IPost, IPostFirebase } from "@warnster/shared-interfaces";

export const transformFirebasePost = (firebasePost: IPostFirebase, postID: string) => {
    const post: IPost = {
        ...firebasePost,
        createdAt: firebasePost.createdAt.toDate(),
        postID,
        comments: []
    }
    return post
}
export const transformFirebaseComment = (firebaseComment: ICommentFirebase, commentID: string) => {
    const comment: IComment = {
        ...firebaseComment,
        createdAt: firebaseComment.createdAt.toDate(),
        commentID,
    }
    return comment
}
