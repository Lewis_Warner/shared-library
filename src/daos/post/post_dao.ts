import { IComment, ICommentBase, ICommentCreate, ICommentFilter, ICommentFirebase, IPost, IPostBase, IPostCreate, IPostFilter, IPostFirebase } from "@warnster/shared-interfaces";
import firebase from 'firebase';
import { transformFirebaseComment, transformFirebasePost } from "../../helpers/post/post_helper";
import Firebase from "../firebase/firebase";
export class PostDao extends Firebase {

    protected posts() {
        return this.firebase.firestore().collection('posts')
    }

    protected comments() {
        return this.firebase.firestore().collection('comments')
    }

    public async createPost(post: IPostBase) {
        const newPost: IPostCreate = {
            ...post,
            createdAt: this.firebase.firestore.FieldValue.serverTimestamp()
        }
        return this.posts().add(newPost)
    }

    public async deletePost(postID: string) {
        this.posts().doc(postID).delete()
    }

    public async deleteComment(commentID: string) {
        this.comments().doc(commentID).delete()
    }

    public async createComment(comment: ICommentBase) {
        const newComment: ICommentCreate = {
            ...comment,
            createdAt: this.firebase.firestore.FieldValue.serverTimestamp()
        }
        return this.comments().add(newComment)
    }

    protected applyPostFilters(postQuery: firebase.firestore.Query<firebase.firestore.DocumentData>, filter: IPostFilter) {
        if (filter.postAuthorID) {
            postQuery = postQuery.where('author.profileID', '==', filter.postAuthorID)
        }
        postQuery = postQuery.orderBy('createdAt', 'desc')
        return postQuery
    }

    public async getPosts(filter: IPostFilter) {
        let postQuery = this.posts().limit(filter.limit)
        postQuery = this.applyPostFilters(postQuery, filter)
        const postSnapshots = await postQuery.get()
        let posts: IPost[] = [];
        postSnapshots.docs.forEach((doc) => {
            const firebasePost = doc.data() as IPostFirebase
            const post = transformFirebasePost(firebasePost, doc.id)
            posts.push(post);
        })
        return posts;
    }

    protected getPostsFromSnapshot(postSnapshot: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) {
        let posts: IPost[] = [];
        postSnapshot.docs.forEach((doc) => {
            const firebasePost = doc.data() as IPostFirebase
            if (doc.metadata.hasPendingWrites === true) {
                const c: IPost = {
                    ...doc.data() as IPostFirebase,
                    createdAt: new Date(),
                    postID: doc.id
                }
                posts.push(c)
                return
            }
            const post = transformFirebasePost(firebasePost, doc.id)
            posts.push(post);
        })
        return posts
    }


    protected applyCommentQuery(commentQuery: firebase.firestore.Query<firebase.firestore.DocumentData>, filter: ICommentFilter) {
        //filter by level
        if (filter.level) {
            commentQuery = commentQuery.where('level', '==', filter.level)
        }
        //Filter by comments belonging to parent
        if (filter.parentID) {
            commentQuery = commentQuery.where('parent', '>=', filter.parentID)
                .where('parent', '<=', `${filter.parentID}~`)
        }
        commentQuery = commentQuery.where('postID', '==', filter.postID).orderBy('createdAt', 'desc')
        return commentQuery
    }

    protected getCommentsFromSnapshot(commentSnapshot: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) {
        let comments: IComment[] = [];
        commentSnapshot.docs.forEach((doc) => {
            const firebaseComment = doc.data() as ICommentFirebase
            if (doc.metadata.hasPendingWrites === true) {
                const c: IComment = {
                    ...doc.data() as ICommentFirebase,
                    createdAt: new Date(),
                    commentID: doc.id
                }
                comments.push(c)
                return
            }
            const comment = transformFirebaseComment(firebaseComment, doc.id)
            comments.push(comment);
        })
        return comments
    }



    public async getComments(filter: ICommentFilter) {
        let commentQuery = this.comments().limit(filter.limit)
        commentQuery = this.applyCommentQuery(commentQuery, filter)
        const commentSnapshots = await commentQuery.get()
        const comments = this.getCommentsFromSnapshot(commentSnapshots)
        return comments;
    }

}