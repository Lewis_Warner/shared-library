import {
    ICheckoutSessionItem, IFirebaseCreatePortalResponse,
    IPlanDetails, IProduct, IProductDetails,
    IProductPrice, IProductUsage
} from '@warnster/shared-interfaces';
import firebase from 'firebase';
import { CurrencyToSymbol } from '../../helpers/user/user_helper';
import Firebase from "../firebase/firebase";

export const PRICING_DETAILS = {
    FREE_TIER_PRODUCT_ID: "prod_J8FMXAY9OcieON",
    FREE_TIER_PRICE_ID: "price_1IVys5FPqvbWkv9Dwn5KeEIe",
    PAID_TIER_PRICE_ID: "price_1IW2ePFPqvbWkv9Dvi8uYhmD"
}

export class Stripe extends Firebase {

    constructor() {
        super("client");
    }

    private getFirebase() {
        return <typeof firebase>this.firebase;
    }

    private getFirestore() {
        return this.getFirebase().firestore();
    }

    public async addCheckoutItem(userID: string, item: ICheckoutSessionItem) {
        return this.getFirestore().collection('users').doc(userID).collection('checkout_sessions').add(item)
    }

    public async getCustomerPortalUrl() {
        const functionRef = this.getFirebase().app().functions('europe-west2').httpsCallable('ext-firestore-stripe-subscriptions-createPortalLink')
        const { data }: { data: IFirebaseCreatePortalResponse } = await functionRef({ returnUrl: window.location.origin })
        return data
    }

    public getUsersSubscription(userID: string) {
        return this.getFirestore().collection('users').doc(userID).collection('subscriptions').where('status', '==', 'active')
    }

    private getFreeProduct() {
        return this.getFirestore().collection('products').doc(PRICING_DETAILS.FREE_TIER_PRODUCT_ID)
    }

    public async getFreePlan(): Promise<IPlanDetails> {
        const snapshot = await this.getFreeProduct().get()
        const priceSnapshot = await this.getFreeProduct().collection('prices').doc(PRICING_DETAILS.FREE_TIER_PRICE_ID).get()
        const productUsageSnapshot = await this.getProductUsages().doc(PRICING_DETAILS.FREE_TIER_PRODUCT_ID).get();
        if (snapshot.exists && priceSnapshot.exists) {
            const product = snapshot.data() as IProduct
            const price = priceSnapshot.data() as IProductPrice
            let usage = {}
            if (productUsageSnapshot.exists) {
                usage = productUsageSnapshot.data() as IProductUsage
            }
            return {
                currency: CurrencyToSymbol[price.currency],
                price: price.unit_amount,
                interval: price.interval,
                image: product.images[0],
                name: product.name,
                usage
            }
        }
        throw Error('Free Subscription not available')
    }

    public getProducts() {
        return this.getFirestore().collection('products').where('active', '==', true)
    }

    public getProductUsages() {
        return this.getFirestore().collection('productUsage')
    }

    public async getProductDetails() {
        const productDocs = await this.getProducts().get();
        const products: { [key: string]: IProduct } = {}
        productDocs.docs.forEach((doc) => {
            products[doc.id] = doc.data() as IProduct
        })
        const productUsageDocs = await this.getProductUsages().get();
        const productUsages: { [key: string]: IProductUsage } = {}
        productUsageDocs.docs.forEach(doc => {
            productUsages[doc.id] = doc.data() as IProductUsage
        })

        const productDetails: IProductDetails[] = []
        let index = 0
        for (const [productID, product] of Object.entries(products)) {
            let hasUsagePlan = false
            let productUsage = {}
            if (productUsages[productID]) {
                hasUsagePlan = true,
                    productUsage = productUsages[productID]
            }
            productDetails.push({ ...product, hasUsagePlan, ...productUsage, productID, id: index })
            index++;
        }

        return productDetails;

    }

    public async createProductUsage(productID: string, productUsage: IProductUsage) {
        return this.getFirestore().collection('productUsage').doc(productID).set(productUsage)

    }

    public async updateProductUsage(productID: string, productUsage: IProductUsage) {
        return this.getFirestore().collection('productUsage').doc(productID).update(productUsage)
    }
}