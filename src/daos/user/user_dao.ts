import { IMessageGroup, IUser, IUserRetrievedFirebase, IUserUpdate, IUserUpdateDraft } from "@warnster/shared-interfaces";
import { transformFirebaseUser } from "../../helpers/user/user_helper";
import Firebase, { TFirebaseNamespace } from "../firebase/firebase";

export class UserDao extends Firebase {

    constructor(type: TFirebaseNamespace) {
        super(type);
    }

    private usersCollection() {
        return this.db.collection("users");
    }

    public async updateUser(userID: string, updateDataDraft: IUserUpdateDraft) {
        const updateData: IUserUpdate = { ...updateDataDraft, updatedAt: this.firebase.firestore.FieldValue.serverTimestamp() }
        return this.usersCollection().doc(userID).update(updateData)
    }

    public async removeUserFromGroup(group: IMessageGroup, userID: string) {
        return this.updateUser(userID, {
            groups: this.firebase.firestore.FieldValue.arrayRemove(group.id)
        });
    }

    public async addUserToGroup(groupID: string, userID: string) {
        return this.updateUser(userID, {
            groups: this.firebase.firestore.FieldValue.arrayUnion(groupID),
        })
    }

    public getUsers(userIDs: string[]) {
        return this.usersCollection().where("uid", "in", userIDs);
    };

    public getUser(userID: string) {
        return this.usersCollection().doc(userID)
    }

    public async findUserByID(userID: string): Promise<IUser | null> {
        try {
            const result = await this.getUser(userID).get();
            if (result.exists) {
                return transformFirebaseUser(result.data() as IUserRetrievedFirebase);
            } else {
                return null;
            }
        } catch (err) {
            console.error({ err });
            return null;
        }
    };

    public onUserUpdate(userID: string, onUpdate: (user: IUser) => void) {
        return this.getUser(userID).onSnapshot((querySnapshot) => {
            const user = transformFirebaseUser(
                querySnapshot.data() as IUserRetrievedFirebase
            );
            onUpdate(user)
        },
            (error) => {
                console.error({ error })
            }
        )
    }

}