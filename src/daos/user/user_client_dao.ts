
import { IUserFilters } from '@warnster/shared-interfaces';
import firebase from 'firebase';
import { UserDao } from './user_dao';

export class UserDaoClient extends UserDao {
    constructor() {
        super("client");
    }

    private getFirebase() {
        return <typeof firebase>this.firebase;
    }

    private getFirestore() {
        return this.getFirebase().firestore();
    }

    public getAuth() {
        return this.getFirebase().auth()
    }

    public async getCurrentUser(): Promise<firebase.User | null> {
        return new Promise((resolve, reject) => {
            this.onAuthStateChanged((user) => {
                return resolve(user)
            });
        })
    }

    public async deleteUser() {
        const user = await this.getCurrentUser();
        if (user) {
            await user.delete()
        }
    }

    public async getProvider() {
        return this.getAuth().currentUser?.providerId
    }

    public onFirebaseIdTokenChanged = (callback: ((a: firebase.User | null) => any)) => {
        this.getAuth().onIdTokenChanged(callback);
    }

    public onAuthStateChanged = (callback: ((a: firebase.User | null) => any)) => {
        this.getAuth().onAuthStateChanged(callback);
    }

    public firebaseLogout = async () => {
        return this.getAuth().signOut()
    }

    public async sendPasswordResetEmail(emailAddress: string) {
        const auth = this.getAuth();
        try {
            auth.sendPasswordResetEmail(emailAddress)
        } catch (err) {
            console.error(err)
        }
    }

    public async confirmPasswordReset(code: string, password: string) {
        const auth = this.getAuth();
        try {
            auth.confirmPasswordReset(code, password)
        } catch (err) {
            console.error(err)
        }
    }

    public getUser(userID: string) {
        return super.getUser(userID) as firebase.firestore.DocumentReference<firebase.firestore.DocumentData>
    }

    public async verifyEmailAddress(code: string): Promise<boolean> {
        try {
            await this.getAuth().applyActionCode(code);
            return true;
        } catch (err) {
            console.error(err)
            return false;
        }
    }

    public async getStripeRole(): Promise<string> {
        //forces refresh to get latest claims and role
        const user = await this.getCurrentUser();
        if (user) {
            const idTokenResult = await user.getIdTokenResult(true)
            return idTokenResult.claims.stripeRole
        }

        return ""


    }

    public async getPaginatedUsers({ startAfter, orderColumn, order, limit, name }: IUserFilters) {
        let usersRef = this.getFirestore().collection("users");
        let query = usersRef
            .orderBy(orderColumn, order)
            .limit(Number(limit))
        if (name) {
            query = usersRef.where("name", "==", name);
        }
        if (startAfter) {
            query = query.startAfter(startAfter);
        }
        return query.get();
    }
}