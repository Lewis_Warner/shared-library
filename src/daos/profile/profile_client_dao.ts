import firebase from 'firebase';
import { ProfileDao } from './profile_dao';

export class ProfileDaoClient extends ProfileDao {
    constructor() {
        super("client");
    }

    private getFirebase() {
        return <typeof firebase>this.firebase;
    }

    private getFirestore() {
        return this.getFirebase().firestore();
    }

    public getAuth() {
        return this.getFirebase().auth()
    }

}