import { IProfile, IProfileFilters, IProfileFirebase, IProfileUpdate, IProfileUpdateDraft } from "@warnster/shared-interfaces";
import { transformFirebaseProfile } from "../../helpers/user/user_helper";
import Firebase, { TFirebaseNamespace } from "../firebase/firebase";

export class ProfileDao extends Firebase {

    constructor(type: TFirebaseNamespace) {
        super(type);
    }

    public profiles() {
        return this.firebase.firestore().collection('profiles')
    }

    public async updateProfile(profile: IProfileUpdateDraft, porfileID: string) {
        const profileUpdate: IProfileUpdate = { ...profile, updatedAt: this.firebase.firestore.FieldValue.serverTimestamp() }
        return this.profiles().doc(porfileID).update(profileUpdate)
    }

    public async getProfileByUserID(userID: string): Promise<IProfile> {
        try {
            const snapshot = await this.profiles().where('userID', '==', userID).get()
            if (snapshot.docs.length === 0) {
                throw new Error('Profile Document for ' + userID + ' doesnt exist')
            }
            const doc = snapshot.docs[0].data();
            const docID = snapshot.docs[0].id
            const profile = transformFirebaseProfile(doc as IProfileFirebase, docID);
            return profile;
        } catch (err) {
            throw err
        }
    }

    public async getProfiles({ ...filters }: IProfileFilters) {
        if (filters.startAt && filters.startAfter) {
            throw Error("can't set both startAt and startAfter")
        }
        let query = this.profiles().where('isComplete', '==', true).limit(filters.limit)
        if (filters.filters.excludeUser) {
            query = query.where('userID', '!=', filters.filters.excludeUser)
        }
        if (filters.startAt) {
            query = query.startAt(filters.startAt)
        }
        if (filters.startAfter) {
            query = query.startAfter(filters.startAfter)
        }
        if (filters.orderColumn && filters.order) {
            query = query.orderBy(filters.orderColumn, filters.order)
        }
        const snapshot = await query.get();
        const profiles: IProfile[] = []
        for (let i = 0; i < snapshot.docs.length; i++) {
            const doc = snapshot.docs[i];
            profiles.push(transformFirebaseProfile((doc.data() as IProfileFirebase), doc.id))
        }
        return profiles
    }

    public async getProfileByID(profileID: string) {
        const snapshot = await this.profiles().doc(profileID).get();
        let profile: IProfile | null = null
        if (snapshot.exists) {
            profile = transformFirebaseProfile(snapshot.data() as IProfileFirebase, snapshot.id);
        }
        return profile;
    }

}