import firebaseClient from 'firebase';


const config = {
    apiKey: process.env.REACT_APP_FIREBASE_PUBLIC_API_KEY || process.env.NEXT_PUBLIC_FIREBASE_PUBLIC_API_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN || process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL || process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID || process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET || process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
    messagingSenderID: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID || process.env.NEXT_PUBLIC_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_APP_ID || process.env.NEXT_PUBLIC_FIREBASE_APP_ID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID || process.env.NEXT_PUBLIC_FIREBASE_MEASUREMENT_ID
};


export type TFirebaseNamespace = "client" | "admin"

export default class Firebase {
    protected firebase: typeof firebaseClient;
    protected db: firebaseClient.firestore.Firestore;
    protected auth: firebaseClient.auth.Auth;
    constructor(type?: TFirebaseNamespace) {
        this.firebase = firebaseClient;
        if (!firebaseClient.apps.length) {

            firebaseClient.initializeApp(config);
            if (process.env.LOCAL_DEV === "development") {
                firebaseClient.firestore().useEmulator("localhost", 8080);
                firebaseClient.auth().useEmulator("http://localhost:9099");
                firebaseClient.functions().useEmulator("localhost", 5001);
            }
            //only call on frontend
            /*if (firebaseClient && firebaseClient.analytics) {
                firebaseClient.analytics()
            }*/
        }

        this.db = this.firebase.firestore();
        this.auth = this.firebase.auth();
    }

    getServerTime() {
        return this.firebase.firestore.FieldValue.serverTimestamp();
    }

    increment(value: number) {
        return this.firebase.firestore.FieldValue.increment(value);
    }
}
