import firebase from 'firebase';
import { ChatDao } from "./chat";

export class ChatDaoClient extends ChatDao {
    constructor() {
        super("client");
    }

    private getFirebase() {
        return <typeof firebase>this.firebase;
    }

    private getFirestore() {
        return this.getFirebase().firestore();
    }

    public getGroups(groupIDs: string[]) {
        return super.getGroups(groupIDs) as firebase.firestore.Query<firebase.firestore.DocumentData>;
    };
}