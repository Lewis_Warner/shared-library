
import { EGroupType, IGroupMemberData, IMessage, IMessageCreate, IMessageFilters, IMessageGroup, IMessageGroupCreate, IMessageGroupUpdate, IMessageRetrievedFirebase, IProfile, TMessageContentType } from '@warnster/shared-interfaces';
import { v4 as uuid } from "uuid";
import { transformFirebaseMessage } from '../../helpers/chat/chat_helper';
import Firebase, { TFirebaseNamespace } from "../firebase/firebase";

export class ChatDao extends Firebase {

    constructor(type: TFirebaseNamespace) {
        super(type);
    }

    public async createGroup(group: IMessageGroupCreate) {
        const groupRef = this.db.collection("groups");
        return await groupRef.add(group);
    }

    public async updateGroupAfterMessage(
        group: IMessageGroupUpdate,
        recipientIDs: string[]
    ) {
        const updateGroup: any = {
            ...group
        }
        recipientIDs.forEach((id) => {
            updateGroup["unread." + id] = this.increment(1);
        });
        return this.updateGroup(group.id, updateGroup);
    };

    protected groupCollection() {
        return this.db.collection("groups");
    }

    public updateGroup(groupID: string, updateData: any) {
        return this.groupCollection().doc(groupID).update(updateData)
    }

    public messageCollection(groupID: string) {
        return this.db.collection("messages")
            .doc(groupID)
            .collection("messages")
    }

    public async addMessage(message: IMessageCreate, groupID: string) {
        return this.messageCollection(groupID)
            .add(message);
    };

    public async getPaginatedMessages(filters: IMessageFilters) {
        if (filters.startAt && filters.startAfter) {
            throw Error("can't set both startAt and startAfter")
        }
        let query = this.messageCollection(filters.filters.groupID).limit(filters.limit)

        if (filters.orderColumn && filters.order) {
            query = query.orderBy(filters.orderColumn, filters.order)
        }
        if (filters.startAt) {
            query = query.startAt(filters.startAt)
        }
        if (filters.startAfter) {
            query = query.startAfter(filters.startAfter)
        }
        const snapshot = await query.get();
        const startAt = snapshot.docs[snapshot.docs.length - 1];
        const messages: IMessage[] = []
        for (let i = 0; i < snapshot.docs.length; i++) {
            const doc = snapshot.docs[i];
            messages.push(await transformFirebaseMessage((doc.data() as IMessageRetrievedFirebase)))
        }
        return { messages, startAt }
    };
    //clear
    public getGroups(groupIDs: string[]) {
        return this.groupCollection()
            .where("id", "in", groupIDs)
            .orderBy("updatedAt", "desc");
    };

    public createMessage(
        body: string,
        senderId: string,
        type: TMessageContentType
    ): IMessageCreate {
        return {
            body: body,
            createdAt: this.getServerTime(),
            senderId: senderId,
            contentType: type,
            id: uuid(),
        };
    }

    public async updateGroupUnread(groupID: string, userID: string) {
        const key = `unread.${userID}`;
        const update = {
            [key]: 0,
        };
        return this.updateGroup(groupID, update);
    };
    //clear
    public createGroupObjects = (
        groupRecipients: IProfile[],
        message: IMessageCreate,
        userID: string,
        groupName = "Private Group Chat"
    ) => {
        const unread: { [key: string]: number } = {};
        const members: { [key: string]: IGroupMemberData } = {};
        groupRecipients.forEach((profile) => {
            if (profile.userID !== userID) {
                unread[profile.userID] = 1;
            } else {
                unread[profile.userID] = 0;
            }
            members[profile.userID] = {
                name: profile.name,
                avatar: profile.avatar,
                isRemoved: false,
                isDeleted: false
            };
        });
        const groupType =
            groupRecipients.length > 2
                ? EGroupType.InviteOnlyGroupChat
                : EGroupType.PrivateChat;
        const newGroupData: IMessageGroupCreate = {
            createdAt: this.getServerTime(),
            createdBy: userID,
            members,
            name: groupName,
            type: groupType,
            avatar: "",
            unread,
            recentMessage: message,
            updatedAt: this.getServerTime(),
        };
        return { newGroupData };
    }

    public async deleteGroup(groupID: string) {
        return this.groupCollection().doc(groupID).delete()
    }

    public addUsersToGroup(profiles: IProfile[], group: IMessageGroup) {
        let updateData: any = {}
        profiles.forEach((profile) => {
            let wasRemoved = false;
            //find users that have removed flag and add them to updateData
            for (const [userID, userData] of Object.entries(group.members)) {
                if (userID === profile.userID && userData.isRemoved === true) {
                    updateData[`members.${profile.userID}.isRemoved`] = false
                    updateData[`unread.${profile.userID}`] = this.firebase.firestore.FieldValue.increment(1);
                    wasRemoved = true;
                }
            }
            //If was not previously removed then add user like normal
            if (wasRemoved === false) {
                const memberData: IGroupMemberData = {
                    name: profile.name,
                    isDeleted: false,
                    isRemoved: false,
                    avatar: profile.avatar
                }
                updateData[`members.${profile.userID}`] = memberData
                updateData[`unread.${profile.userID}`] = 1;
            }
        })
        return updateData;
        //if none then add users like normal and update
        //return db.collection('groups').doc(group.id).update(updateData);
        //return db.collection()
    }

}