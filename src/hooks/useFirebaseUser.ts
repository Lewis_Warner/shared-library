import firebase from 'firebase';
import { useEffect, useState } from "react";
import { UserDaoClient } from "../daos/user/user_client_dao";



export const useFirebaseUser = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [firebaseUser, setFirebaseUser] = useState<firebase.User>();
    const userDao = new UserDaoClient()

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            const data = await userDao.getCurrentUser();
            console.log({ data })
            if (data) {
                setFirebaseUser(data);
            }
            setIsLoading(false);
        };
        fetchData();
    }, []);

    return { isLoading, firebaseUser }
}