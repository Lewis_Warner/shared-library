import { IPost, IPostFilter } from "@warnster/shared-interfaces";
import { useEffect, useState } from "react";
import { PostClientDao } from "../daos/post/post_client_dao";

export const usePosts = (postFilter: IPostFilter) => {
    const [posts, setPosts] = useState<IPost[]>([])

    useEffect(() => {
        //listens for new posts
        const postDao = new PostClientDao()
        const postListener = postDao.onPost(postFilter, (posts) => {
            setPosts(posts)
        })
        return () => {
            postListener()
        }
    }, [])

    return { posts }
}