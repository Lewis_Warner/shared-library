import { IProfile } from "@warnster/shared-interfaces";
import { useEffect, useState } from "react";
import { ProfileDaoClient } from "../daos/profile/profile_client_dao";



export const useProfile = (
    profileID: string) => {
    const [isLoading, setIsLoading] = useState(true);
    const [profile, setProfile] = useState<IProfile>();
    const profileDao = new ProfileDaoClient();

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            const data = await profileDao.getProfileByID(profileID)
            if (data) {
                setProfile(data);
            }
            setIsLoading(false);
        };
        fetchData();
    }, [profileID]);

    return { isLoading, profile }
}