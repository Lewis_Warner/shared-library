import { IProfile, IUseProfiles } from "@warnster/shared-interfaces";
import { useEffect, useState } from "react";
import { ProfileDaoClient } from "../daos/profile/profile_client_dao";


export const useProfiles = ({
    trigger, ...filters
}: IUseProfiles) => {
    const [isLoading, setIsLoading] = useState(true);
    const [profiles, setProfiles] = useState<IProfile[]>([]);
    const profileDao = new ProfileDaoClient();

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            const profiles = await profileDao.getProfiles(filters)
            setProfiles(profiles);
            setIsLoading(false);
        };
        fetchData();
    }, [trigger]);

    return { isLoading, profiles }
}