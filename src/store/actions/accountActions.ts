
import { IImage, IProfile, IProfileUpdate, IProfileUpdateDraft, IUser, IUserRetrievedFirebase } from '@warnster/shared-interfaces';
import firebase from 'firebase';
import { StorageDaoClient } from '../../daos/firebase/storage';
import { ProfileDaoClient } from '../../daos/profile/profile_client_dao';
import { UserDaoClient } from '../../daos/user/user_client_dao';
import { IAppState } from '../../interfaces/settings_interface';

export const LOGIN_SUCCESS = "@account/login-success";
export const SILENT_LOGIN = "@account/silent-login";
export const LOGOUT = "@account/logout";
export const UPDATE_USER_GROUP = "@account/update-user-group";
export const SET_USER = "@account/set-user";
export const SET_PROFILE = "@account/profile/set"
export const SET_PROFILE_IMAGES = "@account/profile/image/set"

export function login(user: IUserRetrievedFirebase): any {
  return (dispatch: any) =>
    dispatch({
      type: SILENT_LOGIN,
      payload: {
        user,
        loggedIn: true,
      },
    });
}

export function setUser(user: IUser) {
  return {
    type: SET_USER,
    payload: { user },
  };
}

export function appendUserGroup(groupID: string) {
  return {
    type: UPDATE_USER_GROUP,
    payload: { groupID },
  };
}

export function logout() {
  return (dispatch: any) => {
    try {
      const userDao = new UserDaoClient();
      userDao.firebaseLogout();
    } catch (err) {
      console.error(err);
    } finally {
      dispatch({
        type: LOGOUT,
      });
    }
  };
}


export function updateProfile(profileUpdate: IProfileUpdateDraft, profileID: string) {
  return (dispatch: any, getState: any) => {
    const profileDao = new ProfileDaoClient();
    const state: IAppState = getState();
    const { profile } = state.account
    const { imagesNew } = profileUpdate;
    delete profileUpdate['imagesNew']
    profileDao.updateProfile(profileUpdate, profileID)
    let images: IImage[] = profile.images
    if (imagesNew) {
      images = [...profile.images, ...imagesNew]
    }
    const newProfile: IProfile = {
      ...profile,
      ...profileUpdate,
      updatedAt: new Date(),
      images
    }
    dispatch(setProfile(newProfile))
  }

}

export function removeProfileImage(image: IImage) {
  const storageDao = new StorageDaoClient()
  const profileDao = new ProfileDaoClient()
  return (dispatch: any, getState: any) => {
    const state: IAppState = getState()
    const { profile } = state.account
    storageDao.deleteFile(image.path)
    const images = profile.images.filter((i) => i.path !== image.path)
    const profileUpdate: IProfileUpdateDraft = {
      images
    }
    if (image.url === profile.avatar) {
      if (profile.images.length > 0) {
        profileUpdate.avatar = profile.images[0].url
      } else {
        profileUpdate.avatar = ""
      }
    }
    //update profile,
    profileDao.updateProfile(profileUpdate, profile.profileID)
    dispatch({
      type: SET_PROFILE_IMAGES,
      payload: { images }
    })
  }
}

export function setProfile(profile: IProfile) {
  return {
    type: SET_PROFILE,
    payload: { profile }

  }
}

export function setProfileAvatar(file: Blob, ext: string) {
  return async (dispatch: any, getState: any) => {
    const storageDao = new StorageDaoClient()
    const state: IAppState = getState()
    const { profile } = state.account
    //upload avatar file
    console.log('uploading blob...');
    const task = await storageDao.uploadFile(file, `profiles/${profile.profileID}`, (uploadTask) => {
    }, ext)
    const image: IImage = {
      url: await task.ref.getDownloadURL(),
      path: task.metadata.fullPath
    }
    console.log('blob uploaded', { image });
    const profileUpdate: IProfileUpdate = {
      avatar: image.url,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp()
    }
    //update firebase
    const profileDao = new ProfileDaoClient()
    profileDao.updateProfile(profileUpdate, profile.profileID)
    //update state profile
    const newProfile: IProfile = {
      ...profile,
      avatar: image.url,
    }
    dispatch(setProfile(newProfile))
  }

}