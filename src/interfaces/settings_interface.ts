import { IChatState, IUserState } from "@warnster/shared-interfaces";
import { DefaultRootState } from "react-redux";

export interface IAppState extends DefaultRootState {
  account: IUserState;
  chat: IChatState;
}

export interface IFileDisplay extends File {
  url?: string
}