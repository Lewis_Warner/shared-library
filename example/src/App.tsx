import { ProfileDaoClient } from '@warnster/shared-library'
import '@warnster/shared-library/dist/index.css'
import React, { useEffect } from 'react'

const App = () => {

  const getProfiles = () => {
    const profileDao = new ProfileDaoClient()
    profileDao.getProfileByUserID("ZR5yMv5bvFXRNe5TXbk2WbB32Ic2").then((profile) => {
      console.log({ profile })
    }).catch((err) => {
      console.error('error ', err)
    })
  }

  useEffect(() => {
  }, [])

  return (<div>
    <button onClick={() => { getProfiles() }}>Get Profile</button>
  </div>)
}

export default App
